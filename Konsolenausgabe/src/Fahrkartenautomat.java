import java.util.Scanner;

class Fahrkartenautomat
{
    public static void main(String[] args)
    {
       Scanner tastatur = new Scanner(System.in);
      

       double eingezahlterGesamtbetrag;
       double eingeworfeneM�nze;
       double r�ckgabebetrag;
       double anzahlTickets; 
       double erhaltendeTickets;
       
       System.out.print("Wieviele Tickets werden ben�tigt?");
       anzahlTickets = tastatur.nextDouble();
       
       System.out.print("Vielen Dank f�r das Angeben Ihrer Ticketanzahl!"+// zuZahlenderBetrag geht weg und wird zu dem hier
       				    "\nF�r den Erhalt des Vorgangs, best�tigen Sie bitte\n"+
       				    "nun den Kauf mit dem Betrag 2,29 Euro.");
       
       erhaltendeTickets = tastatur.nextDouble();
       
       // Beispiel: Wieviele Tickets werden ben�tigt (3 oder eine andere Anzahl)
       // Kauf best�tigen mit 2,29
       // noch zu zahlen 0
       // Eingabe nochmal zur �berpr�fung 2,29
       // Erhalt des Fahrscheins

       // Geldeinwurf
       // -----------
       eingezahlterGesamtbetrag = 0.0;

       {
    	   System.out.println("Noch zu zahlen: " + (eingezahlterGesamtbetrag));
    	   System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
    	   eingeworfeneM�nze = tastatur.nextDouble();
           eingezahlterGesamtbetrag += eingeworfeneM�nze;
       }

       // Fahrscheinausgabe
       // -----------------
       System.out.println("\nFahrschein wird ausgegeben");
       for (int i = 0; i < 8; i++)
       {
          System.out.print("=");
          try {
			Thread.sleep(250);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
       }
       System.out.println("\n\n");

       // R�ckgeldberechnung und -Ausgabe
       // -------------------------------
       r�ckgabebetrag = eingezahlterGesamtbetrag - anzahlTickets;
       if(r�ckgabebetrag > 0.0)
       {
    	   System.out.println("Der R�ckgabebetrag in H�he von " + r�ckgabebetrag + " EURO");
    	   System.out.println("wird in folgenden M�nzen ausgezahlt:" + erhaltendeTickets); //erhaltendetickets befehl hier

           while(r�ckgabebetrag >= 2.29) // 2 EURO-M�nzen
           {
        	  System.out.println("2.29 EURO"); // Beispiel: Eingabe 1,50 Euro, Ausgabe 0,79
	          r�ckgabebetrag -= 2.29;			// Eingabe 0,79 Euro, Ergebnis Fahrschein <- Nachkommastellen
           }
           while(r�ckgabebetrag >= 1.0) // 1 EURO-M�nzen
           {
        	  System.out.println("1 EURO");
	          r�ckgabebetrag -= 1.0;
           }
           while(r�ckgabebetrag >= 0.5) // 50 CENT-M�nzen
           {
        	  System.out.println("50 CENT");
	          r�ckgabebetrag -= 0.5;
           }
           while(r�ckgabebetrag >= 0.2) // 20 CENT-M�nzen
           {
        	  System.out.println("20 CENT");
 	          r�ckgabebetrag -= 0.2;
           }
           while(r�ckgabebetrag >= 0.1) // 10 CENT-M�nzen
           {
        	  System.out.println("10 CENT");
	          r�ckgabebetrag -= 0.1;
           }
           while(r�ckgabebetrag >= 0.05)// 5 CENT-M�nzen
           {
        	  System.out.println("5 CENT");
 	          r�ckgabebetrag -= 0.05;
 	          
 	         System.out.println("3 Tickets");
 	          erhaltendeTickets = 3;
           }
       }

       System.out.println("Vielen Dank f�r die Einzahlung des gew�nschten Betrags!"+
    		   			  "\nVergessen Sie nicht, den Fahrschein\n"+
                          "vor Fahrtantritt entwerten zu lassen!\n"+
                          "Wir w�nschen Ihnen eine gute Fahrt.");
    }
}